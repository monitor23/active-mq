package Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import ObjectRepositories.AMQHomepage;

public class ActiveMQDashboard {
	WebDriver driver;

	@Test
	public void Initializtion() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://admin:admin@172.31.31.171:8161/admin/");
		AMQHomepage amqh = new AMQHomepage(driver);
		amqh.VerifyLogin();
		amqh.VerifyBrokerDetails();
		amqh.Logout();
	}
}

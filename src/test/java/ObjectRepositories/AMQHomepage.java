package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AMQHomepage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AMQHomepage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void VerifyLogin() throws InterruptedException {
		String heading = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".body-content > p:nth-child(2)")))
				.getText();
		// System.out.println(heading);
		if (heading.contains("ID:activemq.netlink-testlabs.com")) {
			System.out.println("Login Successful");

		} else {
			System.out.println("Login Failed");
		}
	}

	public void VerifyBrokerDetails() throws InterruptedException {
		String key, value;
		for (int i = 1; i < 8; i++) {

			Thread.sleep(500);
			key = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector(".body-content > table:nth-child(5) > tbody:nth-child(1) > tr:nth-child(" + i
							+ ") > td:nth-child(1)")))
					.getText();
			value = wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.cssSelector(".body-content > table:nth-child(5) > tbody:nth-child(1) > tr:nth-child(" + i
							+ ") > td:nth-child(2) > b:nth-child(1)")))
					.getText();

			System.out.println(key + " : " + value);
		}
	}

	public void Logout() throws InterruptedException {
		Thread.sleep(1500);
		driver.close();
	}

}
